:: Preamble {{{
:: =============================================================================
::        File: install.cmd
:: -----------------------------------------------------------------------------
::     Authors: Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
:: Maintainers: Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
::  Copyrights: (C) 2014, Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
::     License: This program is free software: you can redistribute it and/or
::              modify it under the terms of the GNU General Public License as
::              published by the Free Software Foundation, either version 3 of
::              the License, or (at your option) any later version.
::
::              This program is distributed in the hope that it will be useful,
::              but WITHOUT ANY WARRANTY; without even the implied warranty of
::              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
::              General Public License for more details.
::
::              You should have received a copy of the GNU General Public
::              License along with this program. If not, see
::              <http://www.gnu.org/licenses/>.
:: =============================================================================
:: }}} Preamble

@echo off

setlocal EnableExtensions

set PATCHES_DIR=%~dp0patches

set VTK_SOURCE_DIR=%1
set VTK_BUILD_DIR=%VTK_SOURCE_DIR%\.build
set VTK_INSTALL_DIR=%2

cd %VTK_SOURCE_DIR%

patch.exe --binary --forward -p0 < "%PATCHES_DIR%\CMakeLists.txt.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\IO\Video\vtkWin32VideoSource.cxx.patch"
patch.exe --binary --forward -p0 < "%PATCHES_DIR%\ThirdParty\libxml2\vtklibxml2\threads.c.patch"

md %VTK_BUILD_DIR%
cd %VTK_BUILD_DIR%

cmake.exe -G Ninja                                                             ^
          -DCMAKE_BUILD_TYPE=Release                                           ^
          -DCMAKE_INSTALL_PREFIX=%VTK_INSTALL_DIR%                             ^
          -DVTK_USE_RENDERING=ON                                               ^
          -DVTK_WRAP_JAVA=ON                                                   ^
          %VTK_SOURCE_DIR%

ninja.exe
ninja.exe "install/strip"

endlocal

:: Modeline {{{
:: =============================================================================
:: vim:ft=dosbatch:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
:: =============================================================================
:: }}} Modeline
